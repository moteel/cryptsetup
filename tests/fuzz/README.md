# Fuzz testing of cryptsetup via [OSS-Fuzz](https://github.com/google/oss-fuzz/)
Basic implementation should work. Currently only one fuzz target is implemented to test cryptsetup API call crypt_load for LUKS1. The corpus archive contains files used as a seed for the fuzzing engine. Specifically, the one already implemented contains LUKS1 headers of images from tests/luks1-images.tar.xz, once without keyslots and once with all of them set. Ideally it should also contain inputs that revealed bugs in the past.

## How to test
[Official instructions](https://github.com/google/oss-fuzz/blob/master/docs/new_project_guide.md)
(Don't forget a folder named cryptsetup containing build.sh, Dockerfile and project.yaml must be in oss-fuzz/projects, as the project is not pulled to oss-fuzz git yet)
```
$ cd /path/to/oss-fuzz
$ python infra/helper.py build_image cryptsetup
$ python infra/helper.py build_fuzzers cryptsetup
$ python infra/helper.py run_fuzzer cryptsetup <fuzz_target>

```
## How to implement
1. merge repository
2. change MAINTAINER and git repo link in Dockerfile
3. pull request files build.sh, Dockerfile and project.yaml to OSS-Fuzz repo

### How to add new fuzz targets
1. write fuzz target named *\_fuzz.cc. Use const _Data_ as a fuzzing input for whatever you want to fuzz. Check the variabe size\_t _size_ against unecessary long inputs.
2. add corpus of input files _(fuzz\_target\_name)_\_seed_corpus.zip
3. [optimize](https://github.com/google/oss-fuzz/blob/master/docs/ideal_integration.md) the target

### minor TODO
* use argv[0] as . path
* remove uneccessary headers
* move mmap to separate function
* call crypt_init() (and maybe mmap) just once

### major TODO
* increase coverage of crypt\_load\_fuzz e.g. by adding more input headers to corpus
* add more fuzz targets to test other parts of cryptsetup
* write crypt\_load fuzz for LUKS2 as well