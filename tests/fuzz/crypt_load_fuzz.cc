extern "C" {
	#define FILESIZE (1049600) 
	#define FILEPATH "/tmp/mmapped.bin"
	#include <stddef.h>
	#include <stdint.h>
	#include <src/cryptsetup.h>
	#include <libcryptsetup.h>
	#include <stdlib.h>
	#include <stdio.h>
	#include <sys/mman.h>
	#include <string.h>
	#include <sys/stat.h>
	#include <unistd.h>
	#include <inttypes.h>
	#include <sys/types.h>
}


extern "C" int LLVMFuzzerTestOneInput(const uint8_t* Data, size_t size) {

//	crypt_set_debug_level(CRYPT_DEBUG_ALL);

	if(size > 592)
		return 0;

	int fd;
	struct crypt_device *cd;
	int result;
	uint8_t *map;

	fd = open(FILEPATH, O_RDWR | O_CREAT | O_TRUNC, (mode_t)0600);
	if (fd == -1)
		exit(EXIT_FAILURE);

	result = lseek(fd, FILESIZE-1, SEEK_SET);
	if (result == -1) {
		close(fd);
		exit(EXIT_FAILURE);
	}
	result = write(fd, "", 1);
	if (result != 1) {
		close(fd);
		exit(EXIT_FAILURE);
	}
	map = (uint8_t *) mmap(0, FILESIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (map == MAP_FAILED) {
		close(fd);
		exit(EXIT_FAILURE);
	}

	memcpy(map, Data, size);

	if (munmap(map, FILESIZE) == -1) {
		close(fd);
		crypt_free(cd);
		exit(EXIT_FAILURE);
	}

	int r = crypt_init(&cd, FILEPATH);
	if (r < 0 ) {
		crypt_free(cd);
		close(fd);
		return 0;
	}

	r = crypt_load(cd, CRYPT_LUKS1, NULL);
	if (r < 0) {
		crypt_free(cd);
		close(fd);
		return 0;
	}

	crypt_free(cd);
	close(fd);
	return 0;
}

